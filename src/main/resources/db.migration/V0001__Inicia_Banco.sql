

create table if not exists documentos
(
    id bigserial not null
        constraint admin_user_pkey
            primary key,
    nome varchar(50) not null

);

create unique index documentos_nome_uindex
    on documentos (nome);
