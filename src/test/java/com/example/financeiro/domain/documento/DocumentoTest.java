package com.example.financeiro.domain.documento;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;

import static com.example.financeiro.domain.documento.Documento.DocumentoBuilder.umDocumento;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@Transactional
@DisplayName("Repositorio de Empresas")
public class DocumentoTest {

    @Autowired
    private TodosOsDocumentos todosOsDocumentos;


    @Test
    public void deve_salvar_um_documento_no_banco(){
        //arrange
        var documentoParaSalvar = umDocumento()
                .comId(null)
                .build();

        //act
        todosOsDocumentos.save(documentoParaSalvar);
        var documentoRecuperado = todosOsDocumentos.findById(documentoParaSalvar.getId()).orElseThrow();

        //ass
        assertEquals(documentoParaSalvar.getNome(), documentoRecuperado.getNome());
    }

    @Test
    public void nao_deve_salvar_com_nome_null(){
        //arrange
        var documentoParaSalvar = umDocumento()
                .comId(null)
                .comNome(null)
                .build();


        //assert
        assertThrows(ConstraintViolationException.class, () -> {
            todosOsDocumentos.save(documentoParaSalvar);
        });
    }

    @Test
    public void nao_deve_salvar_com_nome_com_mais_de_50_caracteres(){
        //arrange
        var documentoParaSalvar = umDocumento()
                .comId(null)
                .comNome("Não deve salvar um nome de documento com mais de 50 caracteres")
                .build();


        //assert
        assertThrows(ConstraintViolationException.class, () -> {
            todosOsDocumentos.save(documentoParaSalvar);
        });
    }
}
