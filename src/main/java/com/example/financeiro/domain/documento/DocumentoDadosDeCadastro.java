package com.example.financeiro.domain.documento;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class DocumentoDadosDeCadastro {

    @NotNull
    @Size(max = 50)
    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public DocumentoDadosDeCadastro() {
    }

    public DocumentoDadosDeCadastro(@NotNull @Size(max = 50) String nome) {
        this.nome = nome;
    }

    public static final class DocumentoDadosDeCadastroBuilder {

        private String nome;

        public static DocumentoDadosDeCadastroBuilder dadosDeCadastro() {
            return new DocumentoDadosDeCadastroBuilder();
        }

        public DocumentoDadosDeCadastroBuilder comNome(String nome) {
            this.nome = nome;
            return this;
        }

        public DocumentoDadosDeCadastro build() {
            return new DocumentoDadosDeCadastro(nome);
        }
    }
}
