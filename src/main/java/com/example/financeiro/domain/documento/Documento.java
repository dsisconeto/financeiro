package com.example.financeiro.domain.documento;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity(name = "documentos")
public class Documento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Size(max = 50)
    @NotNull
    private String nome;

    public Documento(Long id, String name) {
        this.id = id;
        this.nome = name;
    }

    protected Documento() {}

    public Documento(String nome) {
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public static final class DocumentoBuilder {
        private Long id;
        private String nome;

        public static DocumentoBuilder umDocumento() {
            return new DocumentoBuilder();
        }

        public DocumentoBuilder comId(Long id) {
            this.id = id;
            return this;
        }

        public DocumentoBuilder comNome(String nome) {
            this.nome = nome;
            return this;
        }

        public Documento build() {
            Documento documento = new Documento();
            documento.setId(id);
            documento.setNome(nome);
            return documento;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Documento documento = (Documento) o;

        return new EqualsBuilder().append(id, documento.id).append(nome, documento.nome).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(id).append(nome).toHashCode();
    }
}
