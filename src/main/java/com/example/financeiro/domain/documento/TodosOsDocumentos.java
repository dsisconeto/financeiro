package com.example.financeiro.domain.documento;

import org.springframework.data.jpa.repository.JpaRepository;

interface TodosOsDocumentos extends JpaRepository<Documento, Long> {

    boolean existsByNome(String cheque);

    Documento save(Documento documentoSalvo);

    boolean existsByNomeAndIgnoreId(String boleto, Long id);
}
