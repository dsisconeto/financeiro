package com.example.financeiro.api;

import com.example.financeiro.domain.documento.Documento;
import com.example.financeiro.domain.documento.DocumentoService;
import com.example.financeiro.domain.documento.SalvaDocumentoDto;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class DocumentoControllerTest {

    public static final String NOME_COM_MAIS_DE_50_CARACTERES = "ASWERERTRTERGGFGFGFGRETWSADFDSAFDSFGDGFHGDFGFGFDGFGDFHFHGFDGDFGFGDFGDFGFGFG";
    public static final String DUPLICATA = "DUPLICATA";

    @MockBean
    DocumentoService documentoService;

    @Autowired
    MockMvc mvc;

    @Test
    void deve_validar_ter_apenas_50_caracteres_quando_cadastrar() throws Exception {
        // Arrange
        var request = post("/documentos")
                .content("{\"nome\": \"" + NOME_COM_MAIS_DE_50_CARACTERES + "\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8");

        // Act
        var result = mvc.perform(request).andDo(print());

        // Assert
        result
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.errors.nome").value("size must be between 0 and 50"));
    }

    @Test
    void deve_validar_valor_nulo_quando_cadastrar() throws Exception {
        // Arrange
        var request = post("/documentos")
                .content("{}")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8");

        // Act
        var result = mvc.perform(request).andDo(print());

        // Assert
        result
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.errors.nome").value("must not be null"));
    }

    @Test
    void deve_cadastrar_um_documento_quando_valido() throws Exception {
        // Arrange
        var documento = new Documento(1L, DUPLICATA);

        when(documentoService.salva(any(SalvaDocumentoDto.class))).thenReturn(documento);

        var request = post("/documentos")
                .content("{\"nome\": \"" + DUPLICATA + "\"}")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8");

        // Act
        var result = mvc.perform(request).andDo(print());

        // Assert
        result
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.nome").value(DUPLICATA));
    }
}