package com.example.financeiro.domain.documento;

import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.print.Doc;
import java.util.Optional;

@Service
public class DocumentoService {

    private TodosOsDocumentos todosOsDocumentos;

    public DocumentoService(TodosOsDocumentos todosOsDocumentos) {
        this.todosOsDocumentos = todosOsDocumentos;
    }

    public Documento salva(SalvaDocumentoDto salvaDocumentoDto) {
        if (todosOsDocumentos.existsByNome(salvaDocumentoDto.getNome())) {
            throw new NomeUnicoException();
        }

        var documento = new Documento(salvaDocumentoDto.getNome());
        todosOsDocumentos.save(documento);
        return documento;
    }


    public Documento atualiza(Long id, DocumentoDadosDeCadastro dados) {
        if (todosOsDocumentos.existsByNomeAndIgnoreId(dados.getNome(), id)) {
            throw new NomeUnicoException();
        }

        var documentoParaEditar = todosOsDocumentos.findById(id);

        if (documentoParaEditar.isEmpty()) {
            throw new EntityNotFoundException();
        }

        var documentoEmEdicao = documentoParaEditar.get();
        documentoEmEdicao.setNome(dados.getNome());
        todosOsDocumentos.save(documentoEmEdicao);

        return documentoEmEdicao;
    }
}
