package com.example.financeiro.support;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static com.example.financeiro.support.DynamicObject.dynamic;
import static java.util.Objects.nonNull;

@ControllerAdvice
public class HandlerException extends ResponseEntityExceptionHandler {


    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        logger.debug(ex);
        return ResponseEntity
                .badRequest()
                .body(dynamic().with("message", ex.getMessage()));
    }

    @Override
    protected ResponseEntity<Object> handleBindException(
            BindException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request
    ) {
        return createUnprocessableEntityFrom(ex.getBindingResult());
    }


    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request
    ) {
        return createUnprocessableEntityFrom(ex.getBindingResult());
    }


    private ResponseEntity<Object> createUnprocessableEntity(String message, Map<String, String> errors) {
        var body = createBodyForUnprocessableEntity(message, errors);

        return ResponseEntity
                .unprocessableEntity()
                .body(body);
    }

    private ResponseEntity<Object> createUnprocessableEntityFrom(BindingResult bindingResult) {

        return createUnprocessableEntity("Dados inválidos", errorsFrom(bindingResult));
    }


    private Map<String, String> errorsFrom(BindingResult bindingResult) {
        return bindingResult.getFieldErrors()
                .stream()
                .filter(fieldError -> nonNull(fieldError.getDefaultMessage()))

                .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
    }


    private Map<String, Object> createBodyForUnprocessableEntity(String message, Map<String, String> errors) {
        var body = new LinkedHashMap<String, Object>();
        body.put("message", message);
        body.put("errors", errors);
        return body;
    }


}