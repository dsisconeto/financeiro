package com.example.financeiro.domain.documento;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Optional;

import static com.example.financeiro.databuilders.DocumentoDataBuilder.dadosDoCadastroDoDocumento;
import static com.example.financeiro.databuilders.DocumentoDataBuilder.umDocumentoBuilder;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;


class DocumentoServiceTest {

    public static final String CHEQUE = "Cheque";
    public static final String BOLETO = "Boleto";

    @InjectMocks
    DocumentoService documentoService;

    @Mock
    TodosOsDocumentos todosOsDocumentos;

    @BeforeEach
    void inicializaMock() {
        openMocks(this);
        when(todosOsDocumentos.save(any())).thenAnswer(invocation -> invocation.getArgument(0));
    }

    @Test
    void deve_lancar_uma_NomeUnicoException() {
        //Arrange
        var salvaDocumentoDto = new SalvaDocumentoDto(CHEQUE);
        when(todosOsDocumentos.existsByNome(CHEQUE)).thenReturn(true);

        //Assert
        assertThrows(NomeUnicoException.class, () -> {
            documentoService.salva(salvaDocumentoDto);
        });
    }

    @Test
    void deve_salvar_um_documento() {
        //Arrange
        var salvaDocumentoDto = new SalvaDocumentoDto(CHEQUE);
        when(todosOsDocumentos.existsByNome(CHEQUE)).thenReturn(false);

        //Act
        var documentoSalvo = documentoService.salva(salvaDocumentoDto);

        //Assert
        assertEquals(CHEQUE, documentoSalvo.getNome());
        verify(todosOsDocumentos).save(documentoSalvo);
    }

    @Test
    void deve_salvar_um_documento_com_nome_boleto() {
        //Arrange
        var salvaDocumentoDto = new SalvaDocumentoDto(BOLETO);
        when(todosOsDocumentos.existsByNome(BOLETO)).thenReturn(false);

        //Act
        var documentoSalvo = documentoService.salva(salvaDocumentoDto);

        //Assert
        assertEquals(BOLETO, documentoSalvo.getNome());
        verify(todosOsDocumentos).save(documentoSalvo);
    }

    @Test
    void deve_editar_um_documento() {
        //Arrange
        final var dadosRequisicao = dadosDoCadastroDoDocumento()
                .comNome(BOLETO)
                .build();

        var documentoExistente = umDocumentoBuilder()
                .comNome(CHEQUE)
                .build();

        var documentoEmAtualizacao = umDocumentoBuilder()
                .comId(documentoExistente.getId())
                .comNome(dadosRequisicao.getNome())
                .build();

        var documentoAtualizado = umDocumentoBuilder()
                .comId(documentoExistente.getId())
                .comNome(dadosRequisicao.getNome())
                .build();

        when(todosOsDocumentos.existsByNomeAndIgnoreId(BOLETO, documentoExistente.getId())).thenReturn(false);
        when(todosOsDocumentos.findById(documentoExistente.getId())).thenReturn(Optional.of(documentoExistente));
        when(todosOsDocumentos.save(documentoEmAtualizacao)).thenReturn(documentoAtualizado);

        //Act
        var retornoDoDb = documentoService.atualiza(documentoExistente.getId(), dadosRequisicao);

        //Assert
        verify(todosOsDocumentos, times(1)).save(documentoEmAtualizacao);
        assertEquals(BOLETO, documentoExistente.getNome());
        assertThat(retornoDoDb).isEqualTo(documentoAtualizado);
    }

}