package com.example.financeiro.domain.documento;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class SalvaDocumentoDto {
    public SalvaDocumentoDto(String nome) {
        this.nome = nome;
    }

    @Size(max = 50)
    @NotNull
    private String nome;

    public String getNome() {
        return nome;
    }
}
