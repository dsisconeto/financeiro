package com.example.financeiro.api;

import com.example.financeiro.domain.documento.Documento;
import com.example.financeiro.domain.documento.DocumentoService;
import com.example.financeiro.domain.documento.SalvaDocumentoDto;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/documentos")
public class DocumentoController {

    private final DocumentoService documentoService;

    public DocumentoController(
            DocumentoService documentoService
    ) {
        this.documentoService = documentoService;
    }

    @PostMapping
    public Documento salvar(@Valid @RequestBody SalvaDocumentoDto documentoDto) {
        return documentoService.salva(documentoDto);
    }
}
