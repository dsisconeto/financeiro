package com.example.financeiro.support;

import com.fasterxml.jackson.annotation.JsonAnyGetter;

import java.util.LinkedHashMap;
import java.util.Map;

public class DynamicObject {
    private final Map<String, Object> content = new LinkedHashMap<>();


    public static DynamicObject dynamic() {
        return new DynamicObject();

    }

    public DynamicObject with(String field, Object value) {
        content.put(field, value);
        return this;
    }

    @SuppressWarnings("unchecked")
    public <T> T get(String field) {
        return (T) content.get(field);
    }


    @JsonAnyGetter
    public Map<String, Object> getProperties() {
        return content;
    }
}