package com.example.financeiro.support;

import com.github.javafaker.Faker;
import java.util.*;

public class FakerFactory {

    public static Faker faker() {
        return Faker.instance(new Locale("pt-BR"));
    }

}
