package com.example.financeiro.databuilders;

import com.example.financeiro.domain.documento.Documento;
import com.example.financeiro.domain.documento.DocumentoDadosDeCadastro;
import com.github.javafaker.Faker;

import static com.example.financeiro.domain.documento.Documento.DocumentoBuilder.umDocumento;
import static com.example.financeiro.domain.documento.DocumentoDadosDeCadastro.DocumentoDadosDeCadastroBuilder.dadosDeCadastro;
import static com.example.financeiro.support.FakerFactory.faker;

public class DocumentoDataBuilder {
    private static final Faker faker = faker();

    public static Documento.DocumentoBuilder umDocumentoBuilder() {
        return umDocumento()
                .comId(faker.number().randomNumber())
                .comNome(faker.company().name());
    }

    public static DocumentoDadosDeCadastro.DocumentoDadosDeCadastroBuilder dadosDoCadastroDoDocumento() {
        return dadosDeCadastro()
                .comNome(faker.company().name());

    }

}
